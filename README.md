## Trainigssystem Galvanisierung

Ein Projekt von Johanna Meyer und Bennedict Schweimer. Prüfungsleistung des Moduls "Virtuelle Systeme" an der HAW-Hamburg.

Verfügt über einen Demo-Modus, indem die einzelnen Schritte vorgemacht und beschrieben werden und einen Trainings-Modus zum Üben inklusive Feedback.

![Bild 1](Images/GT1.PNG)

![Bild 2](Images/GT2.PNG)
